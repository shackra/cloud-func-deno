import { Router, Request, Response } from "https://deno.land/x/oak/mod.ts";
import { Message } from "https://deno.land/x/telegram_bot_api/mod.ts";

const router = new Router();
router.post(
  "/get_larger_photo_id",
  async ({ request, response }: { request: Request; response: Response }) => {
    const body = request.body();
    const value: Message = await body.value;
    const largerID = value.photo
      ?.map((pic) => {
        return { file_id: pic.file_id, size: pic.width * pic.height };
      })
      .sort((a, b) => {
        if (a.size > b.size) {
          return 1;
        }
        if (a.size < b.size) {
          return -1;
        }

        return 0;
      })
      .pop();

    const hasError = !!largerID;

    response.body = largerID ? largerID.file_id : "";
    response.status = largerID ? 200 : 500;

    if (hasError) console.log({ msg: "hubo un error", value });
  }
);

export default router;
